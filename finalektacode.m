
L1 = 5;
 L2 = 3;
theta1=30; theta2=80;
k=1;
for theta1 = 10:5:100
    for theta2 = 10:5:100

 x0=0;y0=0;

 x1 = L1*cosd(theta1);
 y1 = L2*sind(theta1);

 x2 = x1+L2*cosd(theta1+theta2);
 y2 = y1+L2*sind(theta1+theta2);

 manipul=[[x0 y0];[x1 y1];[x2 y2]];

 fig=plot(manipul(:,1),manipul(:,2),'-o','LineWidth',4);

 axis([-10 10 -10 10]);

 axis square

 figure
 histogram(YTrain)
 axis tight
 ylabel('Counts')
 xlabel('Rotation Angle')

 layers = [
    imageInputLayer([28 28 1])
    convolution2dLayer(3,8,'Padding','same')
    batchNormalizationLayer
    reluLayer
    averagePooling2dLayer(2,'Stride',2)
    convolution2dLayer(3,16,'Padding','same')
    batchNormalizationLayer
    reluLayer
    averagePooling2dLayer(2,'Stride',2)
    convolution2dLayer(3,32,'Padding','same')
    batchNormalizationLayer
    reluLayer
    convolution2dLayer(3,32,'Padding','same')
    batchNormalizationLayer
    reluLayer
    dropoutLayer(0.2)
    fullyConnectedLayer(1)
    regressionLayer];

 miniBatchSize  = 128;
validationFrequency = floor(numel(YTrain)/miniBatchSize);
options = trainingOptions('sgdm', ...
    'MiniBatchSize',miniBatchSize, ...
    'MaxEpochs',30, ...
    'InitialLearnRate',1e-3, ...
    'LearnRateSchedule','piecewise', ...
    'LearnRateDropFactor',0.1, ...
    'LearnRateDropPeriod',20, ...
    'Shuffle','every-epoch', ...
    'ValidationData',{XValidation,YValidation}, ...
    'ValidationFrequency',validationFrequency, ...
    'Plots','training-progress', ...
    'Verbose',false);

 saveas(gcf,['image' num2str(k) '.png'])

 angleInfo(k,:)=[theta1 theta2];

 k=k+1;

    end
end
save ('angleInfo.mat','angleInfo');